/*
 * lupetto - lupetto.js
 * 
 * Description:
 * Functions for lupetto web app, a client for lupus API
 * 
 * License:
 * Copyright 2014 Mònica Ramírez Arceda <monica@probeta.net>
 * 
 * This file is part of lupetto.
 * 
 * lupetto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * lupetto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with lupetto.  If not, see <http://www.gnu.org/licenses/>.
 */

var NODES = [];
var CURRENT_CONFIGS = [];
var CURRENT_SECTIONS = {};
var TIMEOUT = 3000;

$(document).ready(function() {
	$("#addNode").click(getConfigs);
});

/**
 * Add the new node to the list of nodes of the button dropdown 'nodeslist'
 * 
 * @param node the node to be added
 */
function addNodeToList(node) {
	// Create the li item
	var li = $(document.createElement("li")).css("margin-left", "0.5em");
	// Create the remove icon to be able to remove the node
	var removeIcon = $(document.createElement("span")).addClass(
			"glyphicon glyphicon-remove-circle btn btn-default btn-xs active");
	removeIcon.css("margin-left", "0.3em");
	// Remove the node and update configs list
	removeIcon.click(function() {
		var index = NODES.indexOf(node);
		NODES.splice(index, 1);
		updateConfigs();
	});
	// Add the item to the page
	li.append(node).append(removeIcon);
	$("#nodesList").append(li);
}

/**
 * Update configuration files of all nodes added
 */
function updateConfigs() {
	// Reset list of nodes
	$("#nodesList").empty();
	// Empty CURRENT_CONFIGS array
	CURRENT_CONFIGS = [];
	// Reset configs view
	removeConfigs();
	// Get all configs of all nodes
	for ( var i = 0; i < NODES.length; i++) {
		var apiURL = "http://" + NODES[i] + "/cgi-bin/lupus";
		$.ajax({
			url : apiURL + "/uci/get_configs",
			dataType : 'json',
			timeout : TIMEOUT
		}).done(function(data, textStatus, jqXHR) {
			// If everything is ok, update configs and add the node to the list
			removeConfigs();
			prepareConfigs(data);
			showConfigs();
			addNodeToList(this.url.split("/")[2]); // To get the current node,
													// we have to get it from
													// the current URL
		}).fail(
				function(jqXHR, textStatus) {
					// If something failed...
					// Remove the node from the NODES array
					NODES.pop();
					// Depending on the error, give the corresponding message
					if (jqXHR.responseText != "")
						runModal("bg-danger", "Error: " + jqXHR.responseText);
					else
						runModal("bg-danger", "Unable to retrieve data from "
								+ this.url.split("/")[2]
								+ ". Is this node active?");
				});
	}
}

/**
 * Add a node and update configuration files
 */
function getConfigs() {
	// Get the node we want to add
	var node = $("#node").val();
	// Add the node to the nodes array only if it is not already added
	if (jQuery.inArray(node, NODES) == -1) {
		NODES.push(node);
		NODES.sort();
	}
	updateConfigs();
	// Reset and focus the input text to add nodes
	$("#node").val("").focus();
}

/**
 * Get all config file names and build an accordion with them
 * 
 * @param data object with config file
 */
function prepareConfigs(data) {
	// Add all configs of all nodes to CURRENT_CONFIGS and show them in the
	// accordion
	$.each(data["configs"], function(configKey, configValue) {
		// Add the config only if it is not already added
		if (jQuery.inArray(configValue, CURRENT_CONFIGS) == -1) {
			CURRENT_CONFIGS.push(configValue);
		}
	});
	CURRENT_CONFIGS.sort();
}

function showConfigs(data) {
	$.each(CURRENT_CONFIGS, function(configKey, configValue) {
		var configItem = createConfigElement(configValue, configValue
				+ "-heading", configValue + "-body");
		$("#accordion").append(configItem);
		// If we click a config file name, we will show sections for each
		// node
		$("#" + configValue + "-heading").click(function() {
			getSections(configValue);
		});
	});
}

/**
 * Creates a config file item view
 * 
 * @returns an element with the config file item
 */
function createConfigElement(headingText, headingId, bodyId) {
	// Look at http://getbootstrap.com/javascript/#collapse
	// Create the main div
	var panel = document.createElement('div');
	$(panel).addClass("panel").addClass("panel-default");
	// Create the heading div
	var panelHeading = document.createElement('div');
	$(panelHeading).addClass("panel-heading");
	var panelTitle = document.createElement('h4');
	$(panelTitle).addClass("panel-title");
	var titleText = document.createElement('a');
	$(titleText).attr("data-toggle", "collapse").attr("data-parent",
			"#accordion").attr("id", headingId).attr("href", "#" + bodyId)
			.text(headingText);
	$(panelHeading).append(panelTitle);
	$(panelTitle).append(titleText);
	// Create collapsed div with panel body div
	var panelCollapse = document.createElement('div');
	$(panelCollapse).addClass("panel-collapse").addClass("collapse").attr("id",
			bodyId);
	$(panel).append(panelCollapse);
	var panelBody = document.createElement('div');
	$(panelBody).addClass("panel-body");
	$(panelCollapse).append(panelBody);
	// Append heading and body to panel
	$(panel).append(panelHeading);
	$(panel).append(panelCollapse);
	return panel;
}

/**
 * Reset the accordion with configuration files
 */
function removeConfigs() {
	$("#accordion").empty();
}

/**
 * Get all sections of a config file
 * 
 * @param configName a config file name
 */
function getSections(configName) {
	// Reset current sections
	CURRENT_SECTIONS = {};
	// Search sections of this config file for each added node
	$.each(NODES, function(nodeKey, nodeValue) {
		var apiURL = "http://" + nodeValue + "/cgi-bin/lupus";
		$.ajax({
			url : apiURL + "/uci/get_all/" + configName,
			dataType : 'json',
			timeout : TIMEOUT
		}).done(function(sectionData) {
			// Update sections
			removeSections(configName);
			prepareSections(nodeValue, sectionData, configName);
			showSections(configName);
		}).fail(
				function(jqXHR, textStatus) {
					// If connection is lost, show an error
					if (typeof jqXHR.responseText === "undefined") {
						runModal("bg-danger", "Unable to retrieve data from "
								+ this.url.split("/")[2]
								+ ". Is this node active?");
					}
				});
	});
}

/**
 * Fill CURRENT_SECTIONS object with the values of the selected config
 * 
 * @param node a node
 * @param sectionData current section data
 * @param configName current config file name
 */
function prepareSections(node, sectionData, configName) {
	// For each section...
	$.each(sectionData, function(sectionKey, sectionValue) {
		// Create the object if it does not already exists
		if (!(sectionKey in CURRENT_SECTIONS))
			CURRENT_SECTIONS[sectionKey] = {};
		// For each group of options...
		$.each(sectionValue, function(optionKey, optionValue) {
			// Create the object if it does not already exists
			if (!(optionKey in CURRENT_SECTIONS[sectionKey]))
				CURRENT_SECTIONS[sectionKey][optionKey] = {};
			// Fill the section-option value for this node
			CURRENT_SECTIONS[sectionKey][optionKey][node] = optionValue;
		});
	});
}

/**
 * Show all sections of a config file
 * 
 * @param sectionData object containing all sections
 * @param configName the config file name
 */
function showSections(configName) {
	var configPanelBody = $("#" + configName + "-body .panel-body");
	// For each section
	$.each(CURRENT_SECTIONS, function(sectionKey, sectionValue) {
		// Create a section block to display
		$(configPanelBody).append(
				createSectionElement(sectionKey, sectionValue));
		// For each option
		$.each(sectionValue, function(optionKey, optionValue) {
			// Do not display hidden options
			if (optionKey.substring(0, 1) != '.') {
				// Show the corresponding option value for each node
				var firstNode = true;
				$.each(optionValue, function(node, value) {
					$(configPanelBody).append(
							createOptionElement(node, optionKey, value,
									configName, sectionKey, firstNode));
					firstNode = false;
				});
			}
		});
	});
}

/**
 * Creates a section block
 * 
 * @param sectionKey a section name
 * @param sectionValue section object
 * @returns the section block
 */
function createSectionElement(sectionKey, sectionValue) {
	var elem = document.createElement("div");
	$(elem).css("font-weight", "bold");
	$(elem).addClass("section");
	$(elem).attr("id", sectionKey);
	$(elem).text(findType(sectionValue) + " " + sectionKey);
	return elem;
}

/**
 * Find the corresponding type of a section
 * 
 * @param section a section with all its fields
 * @returns the type name
 */
function findType(section) {
	var sectionInterface = undefined;
	var i = 0;
	// Iterate through all the available nodes to see if any of them has this
	// section and get the corresponding type
	while (sectionInterface == undefined && i < NODES.length) {
		sectionInterface = section[".type"][NODES[i]];
		i++;
	}
	return sectionInterface;
}

/**
 * Remove all sections of a config file view
 * 
 * @param configName the name of the config
 */
function removeSections(configName) {
	$("#" + configName + "-body .panel-body").empty();
}

/**
 * Create an option element
 * 
 * @param optionKey an option name
 * @param optionValue the option value
 * @param configKey the corresponding config file name
 * @param sectionKey the corresponding section name
 * @returns the option block
 */
function createOptionElement(node, optionKey, optionValue, configKey,
		sectionKey, firstNode) {
	var optionPath = configKey + "." + sectionKey + "." + optionKey;
	var formGroup = $(document.createElement("div")).addClass("form-group");
	var labelSpan = "";
	if (firstNode) {
		labelSpan = document.createElement("label");
		$(labelSpan).attr("for", optionPath);
		$(labelSpan).addClass("col-md-2 control-label").css("font-weight",
				"normal");
		$(labelSpan).text(optionKey + ": ");
	} else {
		labelSpan = document.createElement("span");
		$(labelSpan).addClass("col-md-2");
	}
	var divInput = $(document.createElement("div")).addClass(
			"col-md-6 input-group");
	var spanNode = $(document.createElement("span")).addClass(
			"input-group-addon").text(node);
	divInput.append(spanNode);
	var input = $(document.createElement("input")).attr("id",
			optionPath + "-" + node).attr("type", "text").attr("value",
			optionValue).addClass("form-control");
	divInput.append(input);
	var buttonOk = $(createButtonOk()).click({
		param1 : optionPath,
		param2 : $(input),
		param3 : node
	}, setOption);
	divInput.append(buttonOk);
	$(formGroup).append(labelSpan).append(divInput);
	return formGroup;
}

/**
 * Create a button with an OK icon
 * 
 * @returns the button
 */
function createButtonOk() {
	var button = $(document.createElement("span")).addClass(
			"input-group-addon btn btn-xs glyphicon glyphicon-ok-sign");
	return button;
}

/**
 * Sets a new value to an option
 * 
 * @param event param1: option name, param2: input element with the new option
 *        value
 */
function setOption(event) {
	var option = event.data.param1;
	var optionValue = event.data.param2.val();
	var node = event.data.param3;
	var apiURL = "http://" + node + "/cgi-bin/lupus";
	var postData = "{\"uci\":{\"" + option + "\":\"" + optionValue + "\"}}";
	$
			.ajax({
				url : apiURL + "/uci/set",
				type : "POST",
				data : postData
			})
			.done(function() {
				runModal("bg-success", "Done!");
			})
			.fail(
					function(jqXHR, textStatus) {
						if (jqXHR.responseText != "")
							runModal("bg-danger", "Error: "
									+ jqXHR.responseText);
						else
							runModal("bg-danger",
									"Error: a weird error has occurred, it may be a bug!");
					});
}

/**
 * Shows a modal to the user
 * 
 * @param className a class to be added to the modal's body
 * @param text the modal text
 */
function runModal(className, text) {
	$("#modal-body").addClass(className).text(text);
	$("#modal").modal();
	setTimeout(function() {
		$('#modal').modal('hide');

	}, 2000);
	$('#modal').on('hidden.bs.modal', function(e) {
		$("#modal-body").removeClass(className);
	});
}
